# Security Projects

The is the home for security projects at the University of Detroit Mercy.
I'm sharing them here in this Gitbook so that my students can find them easier and so other educators can find them.
See the [project list](List.md) for an enumeration of all of the projects that live here.

All projects are available under the license in the Git repo [here](https://gitlab.com/udmercy/security-projects) unless otherwise indicated.
