# Application Security

This project is an exploration of some ways particular programs can be attacked.
In particular, you'll be exploring a few methods to achieve _control-flow hijacking_.
We will be constraining ourselves to exploiting buffer overflows in order to get a bit more
depth on the rest of the process of generating working exploits. 
So beware! These aren't the only way to get your code broken, e.g. use-after free attacks.

## Setup 

You will have to complete this project inside of a Virtual Machine. 
This is because the VM has ASLR defenses turned off so you can more easily complete the project,
and so I can spin up an identical copy and test your submission. This makes everyone's lives easier
and grades more predictable.

If you don't have VM host software, I suggest [VirtualBox](https://www.virtualbox.org/). It is what I use, is cross platform, and has good [documentation](https://www.virtualbox.org/manual/)

Once you have  software to run a virtual machine Download the VM image from [here](https://udmercy0-my.sharepoint.com/:u:/g/personal/vanderba1_udmercy_edu/EaOtKiqR6j5LvVEngzxEQUUB9Yr9RDFa2oTmJz6fGZ7vgw?e=rJf4jW) and import it into your VM host. 

Get the virtual machine up and running: your username is `student` and your password is `password`. 

Next, download and extract the starter zip file inside of the virtual machine. The file is [here](https://udmercy0-my.sharepoint.com/:u:/g/personal/vanderba1_udmercy_edu/EURUZJBr98lDtbkVvwrQNr8BnAbIG0mov3PB7gNPzH2-Rg?e=w89yvJ). 

Open a shell in the extracted directory, the run the command `./config`. When it asks you for your username, enter it. When it asks you for your password, enter `password`. 

You should be all set up now! Run `./test.sh`. This is how I will grade your work. It should say that all of the tests fail and end with your UDMercy username followed by a 0. That zero will increase as you exploit more targets!

If anything to this point is keeping you back, let me know. I will help you get to the meat of the project ASAP. This is a pain to set up, but makes learning the material from here on in easier.



## Exploiting the targets

In the `targets` folder, there are five programs. Your job is to finish the python code in the `exploits` folder to get a root shell for each of those targets.
The way the exploit and target interact varies, so pay close attention to how to test the individual programs. When in doubt, you an just test them all by running the `./test.sh` script.

Bottom line for how to finish the project: 

1. Read the section on the target below
2. Find the vulnerability in the target program (read C code)
3. Figure out how to exploit the vulnerability (gdb is your friend)
4. Write a program to generate the exploit (write Python code)
5. Test the exploit. If you are greeted by a root shell, you are done. (Hash sign on a new line that is waiting for input)
6. Repeat for all five targets.

## Advice

Review lectures. If you want a written resource for the first few targets, check out "Smashing the Stack for Fun and Profit" by Aleph One.

Make sure you are using a x86 assembly resource. This is 32-bit ISA, not x64 which is its 64-bit little brother.

Use gdb. This is a command line tool that lets you jump into and peek around in running code. Some very useful commands are disassemble, info reg, x/128z, break, and stepi. If you want help figuring out how to get GDB to do something, ask. I may know or know where to find out how.

## Targets

These are the five programs you must exploit! Below are a bridf introduction and instructions on how to test each.

### Target 0

This program takes input from stdin and prints a message. Your job is to provide input that causes the program to output: “Hi {USERNAME}! Your grade is A+.” (You should have it output your UDM username without braces.) To accomplish this, your input will need to overwrite another variable stored on the stack.

Some advice: Run the program in gdb with a breakpoint at the start of `_main`. Look at the contents of the stack and registers. Disassemble the `_main` function and look at what instructions are coming next. Step forward. Where do the variables `name[]` and `grade[]` live? Now try breaking after the user enters their username. Do the same. 

You can test your solution by running `python3 exploits/sol0.py | ./targets/target0`. Similarly, you can use your solution in gdb by running `gdb ./targets/target0` and then when you execute the run command in gdb, enter the following: `run < <(python3 ./exploits/sol0.py)`.



### Target 1

This target (and the rest of them) are owned by the root user and have the suid bit set. Your goal is to cause them to launch a root shell. They also accept command line arguments, rather than data from stdin. 

This target is the simplest example of gaining a root shell. All you need to do is to get the shellcode in memory and `eip` pointing to it. 

Warning: x86 is little endian, so if you are trying to put a word into memory, I'd recommend using the pack function we import with format string `"<I"`.

You can test your solution by running `./targets/target1 "$(python3 exploits/sol1.py)"` and run the in gdb with your exploit's output by running `gdb --args ./targets/target1 "$(python3 exploits/sol1.py)"` then just the `run` command with no arguments inside of gdb.

### Target 2

This target is a little harder to exploit. Start by figuring out how to get a buffer overflow.

This target takes as its command-line argument the name of a data file it will read. The file format is a 32-bit count followed by that many 32-bit integers (all little endian). Create a data file that causes the provided shellcode to execute and opens a root shell.

You can test your solution by running `python3 exploits/sol2.py > tmp; ./targets/target2 tmp` and run the in gdb with your exploit's output by running `python3 exploits/sol2.py > tmp; gdb --args targets/target2 tmp"` then just the `run` command with no arguments inside of gdb.

### Target 3 

This target is similar to that of target 1. But there is one huge difference that isn't in the source code. I compiled it with DEP enabled. Try to exploit it anyway. (note that you can't just recompile the program without DEP)

You can test your solution by running `./targets/target3 "$(python3 exploits/sol3.py)"` and run the in gdb with your exploit's output by running `gdb --args ./targets/target3 "$(python3 exploits/sol3.py)"` then just the `run` command with no arguments inside of gdb.

### Target 4

This is a challenging target to exploit. You have to conduct a ROP attack to get your root shell. I've installed the ROPGadget program to help you out.

1. Though there are a number of ways you could implement a ROP exploit, for this target you should use the setuid syscall to become root, followed by the execve syscall to run the `/bin/sh` binary. This is equivalent to:

```
setuid(0);
execve("/bin/sh", 0, 0);
```

2. For an extra push in the right direction, `int 0x80` is the assembly instruction for interrupting execution with a syscall. If the EAX register contains the number 23, the syscall will be `setuid`; if it contains 11, the syscall will be `execve`. You need to figure out what values you need for EBX, ECX, and EDX, and set them using ROP gadgets!

3. I recommend that you start by getting the execve call to work on its own, without setuid. When you do this correctly, it will open a shell, but you won’t be root. Then modify your solution to make it call setuid first, and you’ll get a root shell

## Submission

Submit all five of your solN.py files to the [project grading website](https://autograder.benvds.com).
