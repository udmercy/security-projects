# Cryptography

This project is all about breaking four different types of cryptography. All of these attacks have proven useful in the real world.

## Setup

This project doesn't take place in a VM, fortunately. But you do need to install one library: [pycryptodome](https://pycryptodome.readthedocs.io/en/latest/src/introduction.html).
First, make sure you have `pip` installed. If you don't have it, follow the instructions [here](https://pip.pypa.io/en/stable/installing/).
Then, run the command `pip3 install pycryptodome`. That should be it!

Now download the [project zip file](https://udmercy0-my.sharepoint.com/:u:/g/personal/vanderba1_udmercy_edu/EQ8tQdzRR_NDlra4u78Xsw4BZgbvyIkwm6JlwCSZ9qhpSg?e=tGjfkx), unpack it, and you are ready to go!

## Systems to Attack

This project will have you attacking four different cryptographic systems. 
Each attack has been used to break real systems. So while these look like puzzles,
are fun like puzzles (hopefully), and are probably best approached like puzzles,
techniques like these are absolutely useful.

### Keyed-MAC Length Extension (hashes)

In this scenario, you are to imagine a web service that accepts commands 
in the form of URL parameters that it is sent. To simplify things, we have 
bundled this into the file we provide you `service.py`. We also give you a file 
with some helper functions, `helper.py`.

This web service will produce a request for you if your provide it your username.
The request it produces includes some arguments and ends with a MAC, for example:

```
$ python3 service.py generate vanderba1
command=send;user=vanderba1;item=1%20Metric%20Ton%20Of%20Cookies;reason=None;token=a4ff0fb9c650135da6592839bbff35920dde29ad281d9e2c3a0efb7f7b2a59ab
```

This request represents a request to send me "1 Metric Ton of Cookies" for no reason.
However, if we try to submit that request, the server rejects it for not being sent by an administrator.

```
$ echo "command=send;user=vanderba1;item=1%20Metric%20Ton%20Of%20Cookies;reason=None;token=a4ff0fb9c650135da6592839bbff35920dde29ad281d9e2c3a0efb7f7b2a59ab" | python3 service.py validate vanderba1
No admin bit set, rejected
```

And if we try to send it as an administrator, it can tell that we tampered with our request!

```
$ python3 service.py validate "command=send;user=vanderba1;item=1%20Metric%20Ton%20Of%20Cookies;reason=None;admin=true;token=a4ff0fb9c650135da6592839bbff35920dde29ad281d9e2c3a0efb7f7b2a59ab"
Incorrect token: a4ff0fb9c650135da6592839bbff35920dde29ad281d9e2c3a0efb7f7b2a59ab
Expected token: 592cdc3279946aed589700961be71d2a08b2ef07d4e73a0ff1eda4694f62ca56
```

MACs are generally used to tell if messages have been tampered with, and the token
in the parameter list is just that: a MAC. In particular, it is the hex encoding
of `SHA256( secret_key + decoded_command_message)`. For example, the last failed command above's expected token is
is the result of `SHA256("12345678" + "command=send;user=vanderba1;item=1 Metric Ton Of Cookies;reason=None;admin=true")`.
Instead, it received the MAC from the old message, which did not include the ";admnin=true" portion in the hash input.

Now I've told you the secret key and the output of the service tells you the expected token, so this isn't a super realistic scenario. 
You could just swap the correct token in and be done! Instead, I'll make it more challenging.
You have to write a script that breaks the system regardless of what key it is using.

Put simply, your task is to break the keyed MAC. More precisely
you must write a script that will alter a request that the server validates as
authentic to include admin priveleges while maintaining authenticity.

Write your code in the file `len_ext_attack.py` and then test it by running `./test.sh`.
That will run through the commands I demonstrated above with some more challenging keys.

Fun fact: this attack was used against a handful of websites (Vimeo, Scribd, 
and Flickr to name a few) in 2009.

### Diffie Hellman Key Exchange (kex)

Diffie Hellman key exchange is one of the most commonly used cryptographic tools in the world.
Used without the correct protections, however, it is vulnerable to a Man-in-the-Middle attack.

In this portion of the project a client and server are communicating with each other
about student grades. After exchanging keys, the client sends the password and
a request for student grades to the server. The server validates the password and
responds, also encrypted, the student grades.

The exchange looks like this:

Client and server agree upon password.

Client generates secret key a and derives public key g^a mod p.

Client sends server: p, g, g^a mod p

Server generates secret key b, derives public key g^b mod p, and derives shared secret g^ab mod p .

Server sends client: g^b mod p

Client and server derive symmetric keys: k_c2s, k_s2c = SHA512(g^ab mod p)

Client sends server: ChaCha20(key=k_c2s, password + request)

Server validates request

Server sends client: ChaCha20(key=k_s2c, response)

You have been given the powers of that Man-in-the-Middle attacker. That is to say,
you can read, modify, and drop all messages between the client and server.
You have two tasks: steal the password and modify the grade sent to the client to
be an A.

You are to accomplish this by modifying only the `attacker.py` file. The functions
therein will allow you to read and modify the messages client and server
are exchanging and report what you observed the password to be. This can be tested
by running the `runner.py` program. 

### Stream Cipher Malleability (symmetric)

Hearing that their use of DH was broken, the client and server have switched to just
sharing a secret with each other in advance. Now the exchange looks like this:

Client and server derive symmetric keys: k_c2s, k_s2c = SHA512(shared_secret)

Client sends server: ChaCha20(key=k_c2s, password + request)

Server validates request

Server sends client: ChaCha20(key=k_s2c, response)


This may seem hopeless, but it is not! You have a few things on your side that will help you out.
One, you know that ChaCha20 has ciphertext malleability. Two, you know everyone did so bad in the 
course that the server will respond with "All Students: F". Three, you don't have to figure out the
shared secret anymore.

You are still an Man-in-the-Middle attacker and should still keep your implementation in `attacker.py`.

### DSA Signature Forgery (pubkey)

We saved the best for last! This attack is a terrific example of why nonce really means "Number used ONCE".
This attack is actually how the PS3 was jailbroken (albeit in a different group). 

In the `data/` folder I have given you a hypothetical reading list in text files, 
and to be sure they are not tampered with I have signed them with the Digital Signature Algorithm.
The code that did this is shown in `generator.py` and my public key (which in this hypothetical you would already have had)
is in the file `data/public_key.pem`.

Unfortunately for me, my implementation was broken. It reused the nonce.

Your job is to forge a signature for the reading listed in `data/attack_message.txt`. 
This will obviously ruin my reputation as a scholar. Write the script to create the 
signature in `attack.py` and output the signature itself in `data/attack_message.sig`.
You can verify the signature with `verify.py`.

This attack requires a little bit of number theory, so I will give you the important
information you need to know so you don't have to do any math yourself.

First, note that DSA signatures are just the following math:
In all of the math `g`, `p`, and `q` are specified in `fips-186-3`, a publicly known standard.
Each signer has private key `x` and public key `y=g^x mod p`
Third, a DSA signature has two parts: `r` and `s`.
To make a signature, a random nonce k is chosen, its inverse calculated, and from there `r=g^k mod p` and `s=k^(-1)*(H(m) + x*r)`

Second, if there are two signatures with a repeated nonce, you can easily derive the nonce.
This formula is how: `k = (H(m1)-H(m2)) * ((s1 - s2)^(q-2)) mod q`

Third, if you know the nonce to a signature, you can derive the private key.
This formula is how: `x = ((s * k) - H(m)) * (r^(q-2)) mod q`.

That's it! Given those formulas and my broken implementation, you should be able
to derive my private key and impersonate me to your heart's content.


## Submission

Zip up your project folder with your completed attack files and submit it to the [project grading website](https://autograder.benvds.com).
